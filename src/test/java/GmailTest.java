import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class GmailTest {

    private static Logger LOG = Logger.getLogger(GmailTest.class.getName());
    private WebDriver driver;
    public WebDriverWait wait;

    @BeforeMethod
    public void initializeDriver() {
        DriverManager manager = DriverManager.getInstance();
        driver = manager.openBrowser();
        driver.get("https://mail.google.com");
    }

    @Test
    public void createGoogleTest() {
        WebElement loginInput = driver.findElement(By.id("identifierId"));
        LOG.info("Enter email name");
        loginInput.sendKeys("datsiuk0508@gmail.com");

        LOG.info("Click next button.");
        WebElement nextButton = driver.findElement(By.id("identifierNext"));
        nextButton.click();

        WebElement forgotPasswordButton = (new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOfElementLocated(By.id("forgotPassword")));
        Assert.assertTrue(forgotPasswordButton.isDisplayed());
        LOG.info("Enter password");
        WebElement element = (new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[contains(@type,'password')]")));
        element.sendKeys("88wdxterqwQWER");
        LOG.info("Click next button.");
        WebElement nextPasswordButton = driver.findElement(By.xpath("//*[@id='passwordNext']"));
        nextPasswordButton.click();
//        WebElement gmailSign = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[contains(@class,'gb_jd gb_zd gb_rd')]")));

        LOG.info("Finding  Compose Button");
        WebElement composeButton = driver.findElement(By.xpath(".//div[text()='Compose']"));
        composeButton.click();
        WebElement toInputEmail = driver.findElement(By.xpath("//ancestor::div[contains(@class,'wO nr l1')]//textarea"));
        toInputEmail.sendKeys("datsiuk0508@gmail.com");
        WebElement messageText = driver.findElement(By.name("subjectbox"));
        messageText.sendKeys("Hello Selenium");
        LOG.info("Sent Email Button Click");
        WebElement sendButton = driver.findElement(By.xpath("descendant::div[contains(@class,'dC')]"));
        sendButton.click();
        Assert.assertTrue(driver.findElement(By.id("link_vsm")).isDisplayed(), "Message is sent");
        LOG.info("Message is sent");
    }

    @AfterMethod
    public void driverTearDown() {
        driver.quit();
    }


}